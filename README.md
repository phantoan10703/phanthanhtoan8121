# 2180608295_v1
As a customer, I want to be able to cancel flights before departure time to avoid having to pay

| Title             | Cancel flights                         |
|-------------------|-----------------------------------------------|
| Value Statement   | As a customer, I want to be able to cancel flights before departure time to avoid having to pay |
| Acceptance Criteria| Acceptance Criteriaon 1 :  Cancellation Deadline: Tickets can typically be canceled before the departure time specified on the ticket. The deadline for cancellation is often stated in the terms and conditions. For flights, this is usually a few hours before departure, while for other modes of transportation or events, it can vary.|
| | Acceptance Criteriaon 2  : Cancellation Fee: Many ticket providers charge a cancellation fee. The amount of the fee may depend on how far in advance you cancel the ticket. Some providers offer free cancellation within a certain time frame before departure.|
| | Acceptance Criteriaon 3 : Refund Policy: The acceptance criteria might include information on whether a refund will be issued for canceled tickets. In many cases, you'll receive a refund, but it may be subject to the cancellation fee and the provider's policies.|
| | Acceptance Criteriaon 4 : Cancellation Methods: The ticket provider may specify how you can cancel your ticket. This could be done through their website, a mobile app, or by contacting their customer service. The acceptance criteria may also include information about whether cancellations can be made in person at a ticket counter or office.|
| | Cancellation Methods: The ticket provider may specify how you can cancel your ticket. This could be done through their website, a mobile app, or by contacting their customer service. The acceptance criteria may also include information about whether cancellations can be made in person at a ticket counter or office.|
|  Definition of Done | The cancel flight feature should be implemented and thoroughly tested to ensure it works as expected.
||The user interface should be user-friendly and provide clear confirmation steps.|
||Any applicable refunds or notifications should be triggered automatically.|
||The feature should handle error cases gracefully and provide appropriate error messages to users when necessary.|
| Owner             | Miss Mong Kieu          |
| Iteration | 3 week |
| Estimate| 3 poins |

# 2180607433

|Title| Customer check out and payment|
| Title: | Book tickets |
| :------ | :------ |
| **Value Statement**      | As a _customer_, I want a website that can book tickets quickly to create convenience and comfort for customers       |
| **Acceptance Criteria**        | **Acceptance Criterion 1:** | 
||Users can register with a valid email address and password.| 
||Users receive a verification email upon registration.|
||**Acceptance Criterion 2:** | 
|| Users can search for events, flights, or accommodations using keywords, location, and date.|
||Filter options include price range, date range, and event type|
||Search results are displayed in a clear and organized manner.|
|**Definition of Done**|The feature has been implemented according to the agreed-upon design and specifications|
||All Acceptance Criteria have been met|||
||Accessibility testing has been conducted, and any identified issues have been addressed|
|**Owner**|Miss NgocBich|
|**Iteration**|sprint 3 weeks|
|**Estimate**| 5 days|

![](bookingticket.jpg)

#2180607433
|Title| Customer check out and payment|
|-------------------|-----------------------------------------------|
| Value Statement| As a customer, I would like to be able to pay via bank for more convenience|
| Acceptance Criteria| **_Acceptance Citerion 1:_** <br>Users must be able to add bank account information to make payments <br>The system must verify user bank account information before allowing them to make payments. <br>**_Acceptance Citerion 2:_** <br>The user must be able to select the payment card type. <br>**_Acceptance Citerion 3:_** <br>During the payment process, the system must display all available payment options. <br>**_Acceptance Citerion 4:_** <br>The system must re-verify payment information before confirming the transaction. <br>If there are any issues or errors during the payment process, the system must provide specific error messages to users.|
| Definition of Done|All Acceptance Criteria have been met <br> Security and Data Protection <br>Testing and Verification <br>Unit Test passed <br>Code Review and Quality Assurance|
| Owner| Responsible person: Truong Thanh Dat|

# Demo form
![Capture](https://drive.google.com/uc?export=view&id=1p2ImdcKdsKSCnmSh_e9Jh7VBvSZ_fZGL)



# 2180607502_UserStory1
Application Name: Airline Services Manager
| Title:               | Member Login                              |
|----------------------|-------------------------------------------|
| **Value Statement:**     | As a User with a member account, I want to log in my account so that i can use the airline's services.     |
| **Acceptance Criteria:** | •Acceptance Criterion 1 :             |
|                      | GIVEN the user is registered                                                                    |
|                      | WHEN the user fills in the correct password and username on the login screen                    |
|                      | THEN the system sends a pop-up informing the user that the login is success and directs the user to the home screen with the account info.       |
| **Acceptance Criteria:** | •Acceptance Criterion 2 :             |
|                      | GIVEN the user is registered and forgot the password                                            |
|                      | WHEN the user click the "Forgot Password" button on the login screen                            |
|                      | THEN the system sends a form that requires the user to enter the email used for the account.    |
|                      | WHEN the user enter a valid email to receive a link for password recovery                       |
|                      | THEN the system sends the link to the entered email.                                            |
| **Acceptance Criteria:** | •Acceptance Criterion 3 :             |
|                      | GIVEN the user is registered                                                                    |
|                      | WHEN the user fills in a wrong password or username on the login screen                         |
|                      | THEN the system sends a pop-up informing the user that the username or password is incorrect    |
| **Definition of Done:**  | •Unit Test Passed                     |
|                      | •Acceptance Criteria Met                  |
|                      | •Code Reviewed                            |
|                      | •Fuctional Tests Passed                   |
|                      | •Product Owner Accepts User Story         |
|                      | •Non-Functional Requirements Met          |
| **Owner:**               | Mr.Trung Hiếu                         |
| **Iteration:**           | 2 Weeks                               |
| **Estimate:**            | 3 Points                              |


![Capture](https://drive.google.com/uc?export=view&id=1pbmNFc6-LphqEAWV21wyzaJxcT2kPQhD)

|Title:|Customer change the flight schedule to fit the available flying time.|
|:---|:---|
|Value Statement: |As a **customer**, I want **the ability to modify my flight schedule to align with my preferred flying times**.|
|Acceptance Criteria: |_Acceptance Criterion 1:_<br>The customer should be able to access their current flight schedule through the airline's website or application.<br>_Acceptance Criterion 2:_<br> The system should provide options for the customer to modify their flight schedule, including changing the date and time of the flight.<br>_Acceptance Criterion 3:_<br>The modification feature should allow customers to select flight times that align with their preferred travel times and meet their scheduling needs.|
|Definition of Done: |• Unit Test Passed<br>• Code Reviewed<br>• Functional Tests Passed<br>• Product Owner Accepts User Story|
|Owner: |Toan Phan|
|Iteration: |Unscheduled|
|Estimate: |10 Points|


![example](change_flight.png)

![example](testcase.png)

